package com.example.android.bmicalculator;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class BmiActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String BMI_RESULT = "bmi_result";
    private static final String BMI_STATUS = "bmi_status";
    private static final String BMI_BUTTON_STATUS = "bmi_button_status";
    private static final double FEET_TO_METER = 3.2808;
    private static final double LBS_TO_KG = 2.2046;

    private Spinner mHeightSpinner, mWeightSpinner;
    private LinearLayout bmiLinearLayout;
    private EditText mHeightInFeet, mHeightInInches, mWeight, mHeightInCentimeters;
    private TextView mBmiResult, mStatusOfBmi;
    private Button mCalculateBmi;
    private double result;
    private double height;
    private double weight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        mHeightInFeet = findViewById(R.id.ed_height_feet);
        mHeightInInches = findViewById(R.id.ed_height_inches);
        mWeight = findViewById(R.id.ed_weight);
        mBmiResult = findViewById(R.id.tv_bmi_result);
        mCalculateBmi = findViewById(R.id.button_bmi);
        mHeightSpinner = findViewById(R.id.spinner_height);
        mWeightSpinner = findViewById(R.id.spinner_weight);
        mStatusOfBmi = findViewById(R.id.tv_bmi_status);

        mHeightInCentimeters = findViewById(R.id.ed_centimeteres);
        mHeightSpinner.setOnItemSelectedListener(this);
        bmiLinearLayout = findViewById(R.id.linearLayout);

        mCalculateBmi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculateBodyMassIndex();
            }
        });

        if (savedInstanceState != null) {
            if (savedInstanceState.getInt(BMI_BUTTON_STATUS) == View.VISIBLE) {
                mCalculateBmi.setVisibility(View.VISIBLE);
                mBmiResult.setVisibility(View.INVISIBLE);
                mBmiResult.setVisibility(View.INVISIBLE);
            } else if (savedInstanceState.getInt(BMI_BUTTON_STATUS) == View.GONE) {
                hideKeyboard();
                mHeightInCentimeters.setEnabled(false);
                mHeightInInches.setEnabled(false);
                mHeightInFeet.setEnabled(false);
                mWeight.setEnabled(false);
                mCalculateBmi.setVisibility(View.GONE);
                mStatusOfBmi.setVisibility(View.VISIBLE);
                mBmiResult.setVisibility(View.VISIBLE);
                mBmiResult.setText(savedInstanceState.getString(BMI_RESULT));
                mStatusOfBmi.setText(savedInstanceState.getString(BMI_STATUS));
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(BMI_RESULT, mBmiResult.getText().toString());
        outState.putString(BMI_STATUS, mStatusOfBmi.getText().toString());
        outState.putInt(BMI_BUTTON_STATUS, mCalculateBmi.getVisibility());
        super.onSaveInstanceState(outState);
    }

    public void calculateBodyMassIndex() {
        String heightSpinnerString = mHeightSpinner.getSelectedItem().toString();
        String weightSpinnerString = mWeightSpinner.getSelectedItem().toString();

        if (heightSpinnerString.equals(getString(R.string.cm))) {
            if (!(mHeightInCentimeters.getText().length() > 0)) {
                Toast.makeText(getApplicationContext(), getString(R.string.enterheight), Toast.LENGTH_SHORT).show();
            } else {
                height = Double.parseDouble(mHeightInCentimeters.getText().toString());
                height = height / 100;
                height *= height;
            }
        } else if (heightSpinnerString.equals(getString(R.string.feetheight))) {
            if (!(mHeightInInches.getText().length() > 0)) {
                Toast.makeText(getApplicationContext(), getString(R.string.enterheight), Toast.LENGTH_SHORT).show();
            } else if (!(mHeightInFeet.getText().length() > 0)) {
                Toast.makeText(getApplicationContext(), getString(R.string.enterheightfeet), Toast.LENGTH_SHORT).show();
            } else {
                height = Double.parseDouble(mHeightInFeet.getText().toString()) + (Double.parseDouble(mHeightInInches.getText().toString()) / 10);
                height /= FEET_TO_METER;
                height *= height;
            }
        }

        if (weightSpinnerString.equals(getString(R.string.lbs))) {
            if (!(mWeight.getText().length() > 0)) {
                Toast.makeText(getApplicationContext(), getString(R.string.enterweight), Toast.LENGTH_SHORT).show();
            } else {
                weight = Double.parseDouble(mWeight.getText().toString());
                weight = weight / LBS_TO_KG;
            }
        } else if (weightSpinnerString.equals(getString(R.string.kg))) {
            if (!(mWeight.getText().length() > 0)) {
                Toast.makeText(getApplicationContext(), getString(R.string.enterweight), Toast.LENGTH_SHORT).show();
            } else {
                weight = Double.parseDouble(mWeight.getText().toString());
            }
        }

        if ((mHeightInFeet.getText().length() > 0 && mHeightInInches.getText().length() > 0) || mHeightInCentimeters.getText().length() > 0 && mWeight.getText().length() > 0) {
            result = Math.round(weight / height);
        }

        if (result > 0) {
            hideKeyboard();

            mCalculateBmi.setVisibility(View.GONE);
            mStatusOfBmi.setVisibility(View.VISIBLE);
            mBmiResult.setVisibility(View.VISIBLE);
            mWeight.clearFocus();
            mHeightInFeet.clearFocus();
            mHeightInCentimeters.clearFocus();
            mHeightInInches.clearFocus();
            mBmiResult.setVisibility(View.VISIBLE);
            mStatusOfBmi.setVisibility(View.VISIBLE);
            mHeightInCentimeters.setEnabled(false);
            mHeightInInches.setEnabled(false);
            mHeightInFeet.setEnabled(false);
            mWeight.setEnabled(false);
            mBmiResult.setText(getString(R.string.bmi_is) + " " + Double.toString(result));
            if (result >= 30) {
                mStatusOfBmi.setText(getString(R.string.you_are) + " " + getString(R.string.obese));
            } else if (result >= 25 && result <= 29) {
                mStatusOfBmi.setText(getString(R.string.you_are) + " " + getString(R.string.overweight));
            } else if (result >= 18 && result <= 24) {
                mStatusOfBmi.setText(getString(R.string.you_are) + " " + getString(R.string.normal));
            } else {
                mStatusOfBmi.setText(getString(R.string.you_are) + " " + getString(R.string.under_weight));
            }
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(bmiLinearLayout.getWindowToken(), 0);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();
        if (item.equals(getString(R.string.cm))) {
            mHeightInInches.setVisibility(View.GONE);
            mHeightInFeet.setVisibility(View.GONE);
            mHeightInCentimeters.setVisibility(View.VISIBLE);
            mHeightInCentimeters.requestFocus();
        } else if (item.equals(getString(R.string.feetheight))) {
            mHeightInInches.setVisibility(View.VISIBLE);
            mHeightInFeet.setVisibility(View.VISIBLE);
            mHeightInFeet.requestFocus();
            mHeightInCentimeters.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bmi_chart, menu);
        return true;
    }

    public void resetScreen() {
        mHeightInCentimeters.setEnabled(true);
        mHeightInInches.setEnabled(true);
        mHeightInFeet.setEnabled(true);
        mWeight.setEnabled(true);
        mHeightInFeet.getText().clear();
        mHeightInInches.getText().clear();
        mHeightInCentimeters.getText().clear();
        mWeight.getText().clear();
        mStatusOfBmi.setVisibility(View.GONE);
        mBmiResult.setVisibility(View.GONE);
        mBmiResult.setText("");
        mStatusOfBmi.setText("");
        mCalculateBmi.setVisibility(View.VISIBLE);
        result = 0.0;
        height = 0.0;
        weight = 0.0;
        if (mHeightSpinner.getSelectedItem().toString().equals(getString(R.string.cm))) {
            mHeightInCentimeters.requestFocus();
        } else {
            mHeightInFeet.requestFocus();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemThatWasClickedId = item.getItemId();
        if (itemThatWasClickedId == R.id.action_refresh) {
            resetScreen();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
